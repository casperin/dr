const tape = require('tape')
const copy = require('../1-Copy')

tape('copy - can copy', t => {
    var card = {
        id: 501,
        published: new Date(2010, 6, 26),
        getNextLiveInfo: function(id) {
            return "http://www.dr.dk/mu-online/api/1.0/programcard/" + id;
        },
        title: "Kriminalkommissær Barnaby (3)",
        relatedIds: [544, 958],
        cleanName: new RegExp(/[^a-z0-9]+/gi),
        details: {
            description: "Det er meget virkeligt blod, der drypper på scenen i landsbyens amatørteater. En opførelse af Amadeus slutter dramatisk med, at Salieri skærer halsen over på sig selv. Men nogen har udskiftet teaterkniven med en rigtig, så Barnaby hvirvles ind i et nyt, kompliceret mordmysterium.",
            durationMiliseconds: 1800000
        }
    };

    var clone = copy(card);

    t.notEqual(card, clone, 'not a ref')

    // Number
    t.equal(card.id, clone.id, 'numbers')

    // Date
    t.notEqual(card.published, clone.published, 'date not a ref')
    t.equal(card.published.toString(), clone.published.toString(), 'but same date')

    // Function
    // t.notEqual(card.getNextLiveInfo, clone.getNextLiveInfo)
    t.equal(card.getNextLiveInfo('foo'), clone.getNextLiveInfo('foo'), 'Function should provide same output')

    // String
    t.equal(card.title, clone.title, 'Strings are immutable')

    // Array
    t.notEqual(card.relatedIds, clone.relatedIds, 'Arrays should not be copied by ref')
    t.equal(card.relatedIds[0], clone.relatedIds[0], 'But the numbers inside should')

    // RegExp
    t.notEqual(card.cleanName, clone.cleanName, 'regexp not a ref')
    t.equal(card.cleanName.toString(), clone.cleanName.toString(), 'but same result')

    // Object
    t.notEqual(card.details, clone.details, 'deep clone of objects')
    t.equal(card.details.description, clone.details.description, 'Strings are still immutable')

    /**
     * Destructive tests to make sure that changes in the original does not
     * propagate onto the clone.
     */

    card.id = 42
    t.notEqual(card.id, clone.id, 'id no longer the same')

    card.published.setMonth(10)
    t.notEqual(card.published.toString(), clone.published.toString(), 'Date no longer the same')

    /**
     * Left in to show that I am aware of this "problem".
     *
     *      card.getNextLiveInfo.foo = 'bar'
     *      t.notEqual(
     *          card.getNextLiveInfo.foo,
     *          clone.getNextLiveInfo.foo,
     *          'Fails because the function *is* passed by ref'
     *      )
     *
     * I am curious what was expected here. The problem in my opinion is that
     * we are trying to clone a function (which stems from having a function
     * inside data(!)). We could use eval or new Function for that, but most
     * linters won't allow the use of those (and rightfully so); or if we allow
     * the cloning function to have knowledge of the programCard, then it could
     * call it with an empty string to see what URL it should use to construct
     * its own new function.
     *
     * The obvious real solution is of course to separate data from functions.
     * I'd even go as far as considering objects immutable in general, so that
     * we _can_ copy by reference and still sleep well.
     */

    card.relatedIds[0] = 42
    t.notEqual(card.relatedIds[0], clone.relatedIds[0], 'Ids should no longer be the same')

    // Thanks to Sindre, the regex does not suffer the same problem as the
    // function. Perhaps the problem is the lack of guideline where attaching
    // random values to keys, just because we can, is not allowed.
    card.cleanName.foo = 'bar'
    t.notEqual(card.cleanName.foo, clone.cleanName.foo, 'Regex is not passed by ref')

    card.details.durationMiliseconds = 'very long!'
    t.notEqual(card.details.durationMiliseconds, clone.details.durationMiliseconds)

    t.end()
})
