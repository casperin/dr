const tape = require('tape')
const Coordinate = require('../2-Coordinates')

tape('coordinates - toString()', t => {
    const c = new Coordinate(10, 20)
    t.equal(c.toString(), '{10,20}', 'Should give correct string representation of coordinate')
    t.end()
})

tape('coordinates - parse string', t => {
    const c = new Coordinate('{10,20}')
    t.equal(c.toString(), '{10,20}', 'Should be able to parse string representation')
    t.end()
})

tape('coordinates - parse added coordinates', t => {
    const c = new Coordinate('{1,2}{3,4}')
    t.equal(c.toString(), '{4,6}', 'Should be able to parse and add string representation')
    t.end()
})

tape('coordinates - tests from DR', t => {
    // 1
    const actual1 = new Coordinate(
        new Coordinate(30, 90) +
        new Coordinate(70, 150)
    ).toString()
    const expected1 = '{100,240}'
    t.equal(actual1, expected1, 'Adding actual coordinates should just work')

    // 2
    const actual2 = new Coordinate(
        new Coordinate(250, 10) +
        new Coordinate(60, 430) +
        new Coordinate(80, 0)
    ).toString()
    const expected2 = '{390,440}'
    t.equal(actual2, expected2, 'No really, it should just work')

    t.end()
})
