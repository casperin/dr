
/**
 * Coordinate
 * @public
 * @param {Number|String} x Either the x coordinate as a number, or both
 *                          coordinates as a string representation
 * @param {Number} [y]      Expected to be a number if x is also a number,
 *                          otherwise undefined
 * @returns {Coordinate}
 * Example:
 *      new Coordinate(1, 2)
 *      new Coordinate("{3,4}")
 *
 * To support adding coordinates, the constructor can also take a concatinated
 * string of coordinates as its first argument: new Coordinate("{5,6}{7,8}")
 */
module.exports = Coordinate
function Coordinate (x, y) {
    if (typeof x === 'string' && arguments.length === 1) {
        [x, y] = pairFromStringInput(x)
    }

    if (typeof x !== 'number' || typeof y !== 'number') {
        throw new Error('Could not create Coordinate from x: ' + x + ' and y: ' + y)
    }

    this.x = x
    this.y = y
}

/**
 * @returns {String} A string representation of a coordinate
 */
Coordinate.prototype.toString = function () {
    return `{${this.x},${this.y}}`
}

/**
 * Parses the input in case of a string, and returns a pair of coordinates as
 * an Array.
 * @param {String} input The string to parse
 * @returns {Array}      The resulting pair
 * Example:
 *      pairFromStringInput("{32,12}")          // [32, 12]
 *      pairFromStringInput("{32,12}{42,21}")   // [74, 33]
 */
function pairFromStringInput (input) {
    return input
        .substr(1, input.length - 2)
        .split('}{')
        .map(strToPair)
        .reduce(addPairs)
}

/**
 * Takes two pairs and returns the sum as another pair
 * @param {Array} pair1 First pair of coordinates
 * @param {Array} pair2 Second pair of coordinates
 * @returns {Array}     The resulting pair
 * Example:
 *      addPairs([5, 6], [7, 8]) // [12, 14]
 */
function addPairs ([x1, y1], [x2, y2]) {
    return [x1 + x2, y1 + y2]
}

/**
 * Takes a comma separated list of numbers (as a string) and returns an array
 * with the numbers parsed.
 * @param {String} str The string to be parsed
 * @returns {Array}    Cleaned up array (aka. a "pair" if only two numbers)
 * Example:
 *      strToPair("4,5") // [4, 5]
 */
function strToPair (str) {
    return str.split(',').map(Number)
}
