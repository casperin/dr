const router = require('express').Router()
const request = require('request-promise')
const {send, handleError} = require('../../util')
const {NUM_OF_CARDS_PER_PAGE} = require('../../../../shared/constants')

module.exports = router

const BASE_REQUEST_URL = 'http://www.dr.dk/mu-online/api/1.3/list/view/lastchance'

/**
 * TODO: Make testable
 */
router.get('/', function (req, res) {
    // Defaults to, and lower bound of, 0 (or "page 1")
    const page = Number(req.query.page || 1) - 1
    const offset = Math.max(0, page) * NUM_OF_CARDS_PER_PAGE

    request({
        url: BASE_REQUEST_URL,
        json: true,
        qs: {
            channel: req.query.channel,
            limit: NUM_OF_CARDS_PER_PAGE,
            offset
        }
    })
    .then(send(res))
    .catch(handleError(res))
})
