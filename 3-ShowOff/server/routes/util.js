/**
 * Util functions for routes
 */

exports.send = res => x => res.send(x)

exports.handleError = res => err => {
    console.error(err.stack)
    res.status(500).send('Internal server error!')
}


