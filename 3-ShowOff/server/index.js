const express = require('express')
const api = require('./routes/api')
const app = express()

/**
 * Should be set in .env or similar
 */
const PORT = 8080

/**
 * js, css, images
 */
app.use('/static', express.static(__dirname + '/public/static'))

/**
 * index.html
 */
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/public/index.html')
})

/**
 * api
 */
app.use('/api', api)

app.listen(PORT, function () {
  console.log('Listening on port ' + PORT)
})
