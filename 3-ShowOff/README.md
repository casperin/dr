## My comments

Built with Express/Node backend and React/Redux frontend.

```sh
# Everything should be run from root since that's where package.json lives.

# Install dependencies
npm i

# Build js
npm run build

# Build (and watch) css
npm run css

# Run server
npm start
```

* No tests
    * jsdom and enzyme for the frontend
    * tape for the backend
* May *not* work in ie11
    * Despite the requirement called for it. The reason is simple enough: I
      don't have a machine to test on
* Not "done" per se. A quick brainstorm of TODOs:
    * UX stuff
        * Navigatable by keyboard
        * Aria stuff
        * No-js version
    * Tests
    * propTypes
    * Selectors
    * React router so we keep the channel and page as query params in the url
    * Arrows at the ends of the paginator at the bottom
    * Caching layer for channels/pages (requires a slight change in the redux
      state)

## Assignment

The TV department wants a prototype of how shows that are expiring soon can be presented.

The API endpoint documentation can be found here:
	http://www.dr.dk/mu-online/Help/1.3/Api/GET-api-apiVersion-list-view-lastchance_limit_offset_channel

**Assignment:** Create a simple single page application reading the API endpoint above using front-end technologies and/or Node.js.

There are many ways to solve this assignment, feel free to use templating engines (e.g. Handlebars) or libraries such as React as you see fit.

Write well-structured semantic HTML5 and use CSS3 where it makes sense. The solution should be cross-browser compatible (IE11+) and mobile-friendly.

*You're allowed to keep a static copy of the data feed in your solution to avoid cross-domain issues.*

*Assignment Files:* None, add your own.
