import * as actionTypes from './action-types'
import {NUM_OF_CARDS_PER_PAGE} from '../../shared/constants'

export const updateLastChance = function (channel, page) {
    return (dispatch, getState, {api}) => {
        dispatch({
            type: actionTypes.LAST_CHANCE_FETCHING,
            channel,
            page,
        })

        return api.lastChance.get({channel, page})
        .then(resp => dispatch({
            type: actionTypes.LAST_CHANCE_SUCCESS,
            cards: resp.data.Items,
            pages: Math.ceil(resp.data.TotalSize / NUM_OF_CARDS_PER_PAGE),
            channel,
            page,
        }))
        .catch(err => dispatch({
            type: actionTypes.LAST_CHANCE_FAILURE,
            message: err.message,
        }))
    }
}

