import * as actionTypes from '../action-types'
import {LoadingStatus} from '../constants'

const initialState = {
    channel: 'dr1',
    page: 1,
    pages: -1,
    status: LoadingStatus.NOT_ASKED,
    cards: [],
}

export default (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.LAST_CHANCE_FETCHING:
            return {
                ...state,
                channel: action.channel,
                page: action.page,
                status: LoadingStatus.FETCHING,
            }

        case actionTypes.LAST_CHANCE_SUCCESS:
            return {
                ...state,
                channel: action.channel,
                page: action.page,
                pages: action.pages,
                status: LoadingStatus.SUCCESS,
                cards: action.cards,
            }

        case actionTypes.LAST_CHANCE_FAILURE:
            return {
                ...state,
                status: LoadingStatus.FAILED,
            }

        default:
            return state
    }
}
