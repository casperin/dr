import {combineReducers} from 'redux'
import lastChance from './last-chance'

export default combineReducers({
    lastChance,
})
