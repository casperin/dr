import axios from 'axios'

const BASE = 'api/1.0/last-chance'

function get ({channel, page}) {
    return axios.get(
        BASE,
        {params: {channel, page}}
    )
}

export default {get}
