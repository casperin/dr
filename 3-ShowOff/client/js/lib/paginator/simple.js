import React from 'react'
import classname from 'classname'
import {range} from '../../util/number'

export default props => (
    <div className='paginator-simple'>
        {range(1, props.pages + 1).map(n => (
            <div
                onClick={e => props.onClick(n)}
                key={n}
                className={classname(
                    'paginator-simple-item',
                    {current: n === props.current},
                )}
            >{n}</div>
        ))}
    </div>
)
