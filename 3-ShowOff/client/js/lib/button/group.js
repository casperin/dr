import React from 'react'
import classname from 'classname'

export default props => (
    <div
        className={classname(
            'button-group-wrapper',
            props.className,
        )}
    >
        {props.items.map(item => (
            <button
                key={item.value}
                className={classname(
                    'button-group-button',
                    {current: item.value === props.current},
                )}
                onClick={e => props.onClick(item.value)}
            >{item.text}</button>
        ))}
    </div>
)

