import React from 'react'
import classname from 'classname'

export default props => (
    <div className={classname(
        'program-card-wrapper',
        {loading: props.loading},
        props.className,
    )}>
        {props.children}
    </div>
)

