import React from 'react'

export default props => (
    <a
        href={props.PresentationUri}
        className='program-card-card'
    >
        <div
            className='image'
            style={{backgroundImage: `url(${props.PrimaryImageUri})`}}
        />
        <div className='description'>
            <div className='title'>{props.Title}</div>
            <div className='genre'>{props.OnlineGenreText}</div>
        </div>
    </a>
)
