import React from 'react'
import ReactDom from 'react-dom'
import {Provider} from 'react-redux'
import {createStore, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import reducers from './reducers'
import api from './api'
import App from './components'

let store = createStore(
    reducers,
    applyMiddleware(thunk.withExtraArgument({api})),
)

ReactDom.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.querySelector('.app'),
)
