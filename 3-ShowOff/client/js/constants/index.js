
export const LoadingStatus = {
    NOT_ASKED: 'NOT_ASKED',
    FETCHING: 'FETCHING',
    SUCCESS: 'SUCCESS',
    FAILED: 'FAILED',
}
