
export const range = (min, max) => {
    const result = []
    while (min < max) {
        result.push(min)
        min++
    }
    return result
}
