import React from 'react'
import {connect} from 'react-redux'
import {LoadingStatus} from '../../constants'
import {updateLastChance} from '../../actions'
import ButtonGroup from '../../lib/button/group'
import Loading from '../../lib/loading/small-text'
import ProgramCardWrapper from '../../lib/program-card/wrapper'
import ProgramCard from '../../lib/program-card/card'
import Paginator from '../../lib/paginator/simple'

class LastChance extends React.Component {
    componentWillMount () {
        if (this.props.status === LoadingStatus.NOT_ASKED) {
            this.props.updateLastChance(this.props.channel, this.props.page)
        }
    }

    render () {
        return (
            <div className='last-chance-container'>
                <h1 className='shy-header'>Sidste chance</h1>

                {this.props.status === LoadingStatus.FAILED && (
                    <div className='error-message'>Der skete en fejl! :(</div>
                )}

                <ButtonGroup
                    className='mb1'
                    items={[
                        {text: 'DR', value: 'dr1'},
                        {text: 'DR2', value: 'dr2'},
                        {text: 'DR3', value: 'dr3'},
                    ]}
                    current={this.props.channel}
                    onClick={::this.onChannelChange}
                />

                {this.props.status === LoadingStatus.NOT_ASKED && <Loading />}

                <ProgramCardWrapper
                    loading={this.props.loading}
                    className='mt1'
                >
                    {this.props.cards.map(card =>
                        <ProgramCard key={card.Urn} {...card} />
                    )}
                </ProgramCardWrapper>

                <Paginator
                    pages={this.props.pages}
                    current={this.props.page}
                    onClick={::this.onPageChange}
                />
            </div>
        )
    }

    onChannelChange (channel) {
        this.props.updateLastChance(channel, 1)
    }

    onPageChange (page) {
        this.props.updateLastChance(this.props.channel, page)
    }
}

const mapStateToProps = state => {
    /**
     * We could also use selectors here to isolate the state entirely from
     * components.
     */
    const status = state.lastChance.status
    return {
        channel: state.lastChance.channel,
        page: state.lastChance.page,
        pages: state.lastChance.pages,
        status,
        cards: state.lastChance.cards,
        loading: status === LoadingStatus.NOT_ASKED || status === LoadingStatus.FETCHING,
    }
}

export default connect(mapStateToProps, {updateLastChance})(LastChance)

