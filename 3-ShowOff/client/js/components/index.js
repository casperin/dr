import React from 'react'
import LastChance from './last-chance'

export default class App extends React.Component {
    render () {
        return (
            <div>
                <LastChance />
            </div>
        )
    }
}

