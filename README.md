# My comments

The tasks are solved and commented on in the README of the individual
directories, however tests are run with one command and are there for first and
second assignment.

```sh
npm install
npm test
```

# Front-end Applicant Assignments

This repository contains 3 assignments for you to complete before your interview at DR. All assignments should not take more than a few hours to complete, but feel free to spend as much time as you feel is necessary to showcase your talents. There are no definitively "correct" solutions. *Also remember that this is not mandatory. If you've got some other code you would rather talk about (e.g. on Github) then simply send us a link to that instead.*

| Assignment
|:-----------------
| 1. Copy
| 2. Coordinates
| 3. ShowOff

## How to submit your results

1. [Download this repository](https://github.com/drdk/applicant-assignment-frontend/archive/master.zip) and add your solution files to each respective assignment folder.
2. When you are done, send it to us by email, either as a link or attached as an archive.
