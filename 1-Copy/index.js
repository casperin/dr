const cloneRegexp = require('clone-regexp')

module.exports = deepCopy

function deepCopy (x) {
    if (typeof x !== 'object') return x
    if (x instanceof Date) return new Date(+x)
    if (x instanceof RegExp) return cloneRegexp(x)
    if (x instanceof Array) return x.map(deepCopy)

    // Assuming a normal object with keys and such
    return Object
        .keys(x)
        .reduce((o, key) => {
            o[key] = deepCopy(x[key])
            return o
        }, {})
}
